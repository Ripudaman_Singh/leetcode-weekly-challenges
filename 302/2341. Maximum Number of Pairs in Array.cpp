// just use map to find frequency of numbers

class Solution {
public:
    vector<int> numberOfPairs(vector<int>& nums) {
        unordered_map<int, int> m;
        for (int i = 0; i < nums.size(); i++) {
            m[nums[i]]++;
        }
        
        int pairs = 0;
        for (auto i: m) {
            pairs += i.second/2;
        }
        
        vector<int> res(2, -1);
        res[0] = pairs;
        res[1] = nums.size() - (pairs*2);
        return res;
    }
};