// use map to store numbers with same digit sum

class Solution {
    int sum(int digit) {
        int s = 0;
        while (digit != 0) {
            s += digit%10;
            digit = digit/10;
        }
        return s;
    }
public:
    int maximumSum(vector<int>& nums) {
        sort(nums.begin(), nums.end(), greater<int>());
        unordered_map<int, vector<int>> m;
        
        for (int i = 0; i < nums.size(); i++) {
            m[sum(nums[i])].push_back(nums[i]);
        }
        
        int max_val = -1;
        for (auto i: m) {
            if (i.second.size() < 2)
                continue;
            max_val = max(max_val, i.second[0] + i.second[1]);
        }
        return max_val;
    }
};
